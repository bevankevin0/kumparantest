//
//  KumparanTest2Tests.swift
//  KumparanTest2Tests
//
//  Created by bevan christian on 04/01/22.
//

import XCTest
import Combine
@testable import KumparanTest2


class KumparanTest2Tests: XCTestCase {

    var subscription = Set<AnyCancellable>()
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

  
    
    func testUrlEnpoint() {
        // Testing our endpoint url
        let urlAlbums = UrlAddress.albums(2).urlString
        let urlAlbumsPhoto = UrlAddress.albumPhoto(3).urlString
        
        let correctUrlAlbums = "https://jsonplaceholder.typicode.com/users/2/albums"
        let correctUrlAlbumsPhoto = "https://jsonplaceholder.typicode.com/albums/3/photos"
        
        XCTAssertEqual(urlAlbums, correctUrlAlbums)
        XCTAssertEqual(urlAlbumsPhoto, correctUrlAlbumsPhoto)
    }
    
    func testPostValidResponse() {
        // given mock api
        let apiMock = ApiMock()
        // when case is success
        apiMock.cases = .succes
        let request = HomeService(env: RequestUtils(apiRequest: apiMock))
        // then message error must nil
        request.getPostData().sink { Completion in
            switch Completion {
            case .finished:
            print("finished test")
            case .failure(_):
            print("error test")
            }
        } receiveValue: { value in
            XCTAssertEqual(true, value)
        }.store(in: &subscription)

    }
    
    func testPostInvalidResponse() {
        // given mock api
        let apiMock = ApiMock()
        // when case is invalid data
        apiMock.cases = .invalidData
        let request = HomeService(env: RequestUtils(apiRequest: apiMock))
        // then message must contain error enpoint
        
        request.getPostData().sink { Completion in
            switch Completion {
            case .finished:
            print("finished test")
            case .failure(let err):
            print("error test \(err)")
            XCTAssertNotNil(err)
            }
        } receiveValue: { value in
   
        }.store(in: &subscription)

       
    }

}
