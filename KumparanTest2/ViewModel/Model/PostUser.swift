//
//  PostUser.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation

struct PostUser {
    var idUser: Int
    var titleContent: String
    var bodyContent: String
    var name: String
    var company: String
}

