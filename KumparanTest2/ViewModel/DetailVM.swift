//
//  DetailVM.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine
import SwiftUI

class DetailVM: ObservableObject {
    @Published var commentData = [Comment]()
    let service: DetailServiceProtocol
    var subscription = Set<AnyCancellable>()

    init(service: DetailServiceProtocol = DetailService(env: RequestUtils(apiRequest: ApiReal()))) {
        self.service = service
        observeData()
    }
    
    func getData(idPost: Int) {
        service.getComments(idPost: "\(idPost)")
    }
    
    private func observeData() {
        service.commentResult.sink {[weak self] data in
            self?.commentData = data
        }.store(in: &subscription)
    }
}
