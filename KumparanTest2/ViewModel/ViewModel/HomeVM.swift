//
//  HomeVM.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine
import SwiftUI


class HomeVM: ObservableObject {
    @Published var postUser = [PostUser]()
    @Published var postData = [Post]()
    @Published var userData = [User]()
    let service: HomeServiceProtocol
    var subscription = Set<AnyCancellable>()
    
    init(service: HomeServiceProtocol = HomeService.shared) {
        self.service = service
        observeData()
        load()
        mapping()
    }

    func load() {
        getPost()
        getUsers()
    }
    
    private func observeData() {
        service.postResult.sink {[weak self] data in
            self?.postData = data
            self?.getUsers()
        }.store(in: &subscription)
        
        service.userResult.sink { [weak self] userData in
            self?.userData = userData
        }.store(in: &subscription)
    }
    
    private func mapping() {
        $postData.zip($userData).sink { postUser, userData in
            self.postUser.removeAll()
            for post in postUser {
                let user = userData.first { user in
                    user.id == post.userId
                }
                let mergingData = PostUser(id: post.id ?? 0, titleContent: post.title ?? "", bodyContent: post.body ?? "", usernme: user?.username ?? "", company: user?.company?.name ?? "")
                self.postUser.append(mergingData)
            }
            print("hasil merge \(self.postUser)")
        }.store(in: &subscription)
    }
    
  
}

extension HomeVM {
    private func getPost() {
        service.getPostData()
    }
    
    private func getUsers() {
        service.getUsers()
    }
}
