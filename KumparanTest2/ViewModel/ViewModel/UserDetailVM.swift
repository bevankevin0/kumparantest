//
//  UserDetailVM.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine
import SwiftUI

class UserDetailVM: ObservableObject {
    @Published var user = [User]()
    @Published var album = [Album]()
    @Published var albumPhoto = [AlbumPhoto]()
    @Published var finalData = UserDetailModel(name: "", email: "", address: "", company: "", album: AlbumList(name: "", photoName: [""], thumbnail: [""]))
    var service: UserDetailProtocol
    var subscription = Set<AnyCancellable>()

    init(service: UserDetailProtocol = UserDetailService.shared) {
        self.service = service
        observeData()
        mappingResult()
    }
    
    func getData(idPost: Int) {
        service.getUserDetailData(id: idPost)
    }
    
    private func observeData() {
        service.userResult.sink {[weak self] data in
            self?.user = data
            print("user detail data  \(data)")
        }.store(in: &subscription)
        
        service.albumResult.sink {[weak self] data in
            self?.album = data
            print("album data  \(data)")
        }.store(in: &subscription)
        
        service.albumPhotoResult.sink {[weak self] data in
            self?.albumPhoto = data
            print("album photo data  \(data)")
        }.store(in: &subscription)
    }
    
    private func mappingResult() {
        $user.zip($album, $albumPhoto).sink { user, albumdata, albumphotodata in
            var newAlbum = AlbumList(name: "", photoName: [""], thumbnail: [""])
            var mergeData = UserDetailModel(name: user.first?.name ?? "", email: user.first?.email ?? "", address: user.first?.address?.street ?? "", company: user.first?.company?.name ?? "", album: AlbumList(name: "", photoName: [""], thumbnail: [""]))
            for x in albumdata {
                newAlbum = AlbumList(name: x.title ?? "", photoName: [""], thumbnail: [""])
                for y in albumphotodata {
                    newAlbum.photoName.append(y.title ?? "")
                    newAlbum.thumbnail.append(y.thumbnailUrl ?? "")
                }
               
            }
            mergeData.album = newAlbum
            self.finalData = mergeData
            print("hasil ahkir mapping \(mergeData)")
        }.store(in: &subscription)
    }
    
    
}
