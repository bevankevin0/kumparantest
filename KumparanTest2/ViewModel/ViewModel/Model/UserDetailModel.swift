//
//  UserDetailModel.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation

struct UserDetailModel {
    var name: String
    var email: String
    var address: String
    var company: String
    var album: AlbumList
}

struct AlbumList {
    var name: String
    var photoName: [String]
    var thumbnail: [String]
}
