//
//  PostUser.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation

struct PostUser: Identifiable {
    var id: Int
    var titleContent: String
    var bodyContent: String
    var usernme: String
    var company: String
}
