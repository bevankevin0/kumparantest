//
//  RequestUtils.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine

protocol ApiProtcol {
    func request(url:URLRequest) -> AnyPublisher<Data, Never>
}

protocol RequestUtilsProtocol {
    func request<T:Codable>(urlComponent: URLComponents,model: T.Type) -> Future<T,ApiError>
}

class RequestUtils: RequestUtilsProtocol {
    var subscription = Set<AnyCancellable>()
    var requestApi: ApiProtcol
    
    init(apiRequest: ApiProtcol = ApiReal()) {
        requestApi = apiRequest
    }

    
    func request<T:Codable>(urlComponent: URLComponents,model: T.Type) -> Future<T,ApiError> {
        return Future() { [self] promise in
            guard let urlFix = try? URLRequest(url: (urlComponent.url)!) else {return}
            requestApi.request(url: urlFix)
               .decode(type: model.self, decoder: JSONDecoder())
               .receive(on: DispatchQueue.main)
               .sink { completion in
                   switch completion {
                   case .finished:
                       print(completion)
                   case .failure(_):
                       print("error")
                       promise(.failure(.apiError))
                   }
               } receiveValue: { hasil in
                   promise(.success(hasil))
               }.store(in: &subscription)
        }
    }
}




