//
//  UrlAddress.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation

enum UrlAddress {
    case post
    case user
    case comment
    case albums(Int)
    case albumPhoto(Int)
    var urlString: String {
        switch self {
        case .post:
            return "https://jsonplaceholder.typicode.com/posts"
        case .user:
            return "https://jsonplaceholder.typicode.com/users"
        case .comment:
            return "https://jsonplaceholder.typicode.com/comments"
        case .albums(let id):
            return "https://jsonplaceholder.typicode.com/users/\(id)/albums"
        case .albumPhoto(let id):
            return "https://jsonplaceholder.typicode.com/albums/\(id)/photos"
            
        }
    }
    
}
