//
//  UserDetailPage.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import SwiftUI
import Kingfisher

struct UserDetailPage: View {
    @StateObject var userDetailVm = UserDetailVM()
    var userId: Int
    var body: some View {
        VStack(alignment: .center,spacing: 20) {
            Text(userDetailVm.finalData.name)
                .font(.title2)
                .bold()
            Text("Email: \(userDetailVm.finalData.email)")
                .bold()
                .font(.subheadline)
            Text("Address : \(userDetailVm.finalData.address)")
                .font(.subheadline)
            Text("Company Name : \(userDetailVm.finalData.company)")
                .font(.subheadline)
            Spacer()
            Divider()
            Text("ALBUM:")
            Text(userDetailVm.finalData.album.name)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ForEach(0..<userDetailVm.finalData.album.thumbnail.count,id:\.self) { row in
                        HStack {
                            NavigationLink {
                                PhotoView(imageUrl: userDetailVm.finalData.album.thumbnail[row], title: userDetailVm.finalData.album.photoName[row])
                            } label: {
                                KFImage(URL(string: userDetailVm.finalData.album.thumbnail[row]))
                                    .placeholder({
                                        Image("placeholder")
                                            .resizable()
                                            .frame(width: 100, height: 100)
                                    })
                                    .resizable()
                                    .frame(width: 100, height: 100)
                            }
                        }
                        
                    }
                }
                
            }.frame(height: 150)
                .navigationTitle(Text("Detail User Page"))
        }.onAppear {
            userDetailVm.getData(idPost: userId)
        }
    }
}

struct UserDetailPage_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailPage(userId: 1)
    }
}
