//
//  DetailView.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import SwiftUI

struct DetailView: View {
    @StateObject var detailVm = DetailVM()
    var postData: PostUser
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .leading) {
                NavigationLink {
                    UserDetailPage(userId: postData.idUser)
                } label: {
                    VStack(alignment:.leading) {
                        PostCardView(fromDetail: true, data: postData)
                    }
                    
                }.buttonStyle(PlainButtonStyle()).padding()
                Spacer()
                Divider()
                VStack(alignment: .leading, spacing:20) {
                    Text("Comment Section")
                        .font(.title3)
                        .bold()
                        .padding()
                    List(detailVm.commentData) { row in
                        CommentComponentView(commentData: row)
                    }
                }
                Spacer()
            }
            .navigationTitle(Text("Detail Post Page"))
            .navigationBarTitleDisplayMode(.inline)
        }.onAppear {
            detailVm.getData(idPost: postData.idUser)
        }
        
    }
}


struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(postData: PostUser(idUser: 1, titleContent: "judul", bodyContent: "body", name: "username", company: "company name"))
    }
}
