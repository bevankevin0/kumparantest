//
//  commentComponentView.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import SwiftUI

struct CommentComponentView: View {
    var commentData: Comment
    var body: some View {
        VStack(alignment: .leading, spacing: 30) {
            Text(commentData.body ?? "")
                .font(.body)
            Text("Author name \(commentData.name ?? "")")
                .font(.caption)
                .foregroundColor(.gray)
        }.padding()
    }
}

struct commentComponentView_Previews: PreviewProvider {
    static var previews: some View {
        CommentComponentView(commentData: Comment())
    }
}
