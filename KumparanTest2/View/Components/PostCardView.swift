//
//  PostCardView.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import SwiftUI

struct PostCardView: View {
    var fromDetail = false
    var data: PostUser
    var body: some View {
        VStack(alignment:.leading,spacing:10) {
            HStack {
                Text(data.titleContent)
                    .font(.title2)
                    .bold()
                    .multilineTextAlignment(.leading)
                Spacer()
            }
            HStack {
                Text(data.bodyContent)
                    .font(.body)
                    .multilineTextAlignment(.leading)
                Spacer()
            }
 
            HStack(spacing: 20) {
                Text("By: \(data.name)")
                Text(data.company)
            }
            .font(fromDetail ? .title2 :.caption)
            .foregroundColor(fromDetail ? .blue :.gray)
            
        }
        .padding()
    }
}

struct PostCardView_Previews: PreviewProvider {
    static var previews: some View {
        PostCardView(data: PostUser(idUser: 1, titleContent: "title", bodyContent: "body bos", name: "bevan", company: "abc company"))
    }
}
