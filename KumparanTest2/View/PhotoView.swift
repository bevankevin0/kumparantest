//
//  PhotoView.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import SwiftUI
import Kingfisher

struct PhotoView: View {
    @State var currentScale: CGFloat = 0
    @State var finalScale: CGFloat = 1
    var imageUrl: String
    var title: String
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .center) {
                KFImage(URL(string: imageUrl))
                    .placeholder({
                        Image("placeholder")
                            .resizable()
                            .frame(width: geo.size.width * 1, height: geo.size.height * 0.5)
                    })
                    .resizable()
                    .frame(width: geo.size.width * 1, height: geo.size.height * 0.5)
                    .scaleEffect(finalScale + currentScale)
                    .gesture(MagnificationGesture().onChanged({ newScale in
                        currentScale = newScale
                    }).onEnded({ scale in
                        finalScale = scale
                        currentScale = 0
                    }))
                Spacer()
                Text("\(title)")
                    .font(.title)
                    .bold()
                Spacer()
                .navigationTitle(Text("Photo Detail Page"))
            }
        }.padding()
    }
}

struct PhotoView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoView(imageUrl: "", title: "hello")
    }
}

