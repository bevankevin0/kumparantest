//
//  ContentView.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var homeVm = HomeVM()
    @State var isLoading = true
    var body: some View {
        NavigationView {
            ZStack {
                VStack(alignment:.leading,spacing:10) {
                    List(homeVm.postUser,id:\.bodyContent) { row in
                        NavigationLink {
                            DetailView(postData: row)
                        } label: {
                            PostCardView(data: row)
                        }
                    }
                    .listStyle(PlainListStyle())
                    .refreshable {
                        homeVm.load()
                    }
                }
                if isLoading {
                    ProgressView()
                    
                }
            }
            .navigationTitle(Text("Homepage"))
        }.onReceive(homeVm.$postUser) { value in
            if value.count > 1 {
                isLoading = false
            } else {
                isLoading = true
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
