//
//  AlbumPhoto.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation


struct AlbumPhoto: Codable {
    var albumId, id: Int?
    var title: String?
    var url, thumbnailUrl: String?
}
