//
//  Comment.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation

struct Comment: Codable, Identifiable {
    var postId, id: Int?
    var name, email, body: String?
}
