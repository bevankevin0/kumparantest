//
//  UserDetailProtocol.swift
//  KumparanTest2
//
//  Created by bevan christian on 04/01/22.
//

import Foundation
import Combine


protocol UserDetailProtocol {
    var userResult: CurrentValueSubject<[User], Never> { get }
    var albumResult: CurrentValueSubject<[Album], Never> { get }
    var albumPhotoResult: CurrentValueSubject<[AlbumPhoto], Never> { get }
    func getUserDetailData(id: Int)
}
