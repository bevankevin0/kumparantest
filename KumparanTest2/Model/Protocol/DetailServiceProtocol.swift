//
//  DetailServiceProtocol.swift
//  KumparanTest2
//
//  Created by bevan christian on 04/01/22.
//

import Foundation
import Combine

protocol DetailServiceProtocol {
    var commentResult: CurrentValueSubject<[Comment], Never> { get }
    func getComments(idPost: String)
}
