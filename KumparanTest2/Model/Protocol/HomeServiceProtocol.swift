//
//  HomeServiceProtocol.swift
//  KumparanTest2
//
//  Created by bevan christian on 04/01/22.
//

import Foundation
import Combine

protocol HomeServiceProtocol {
    func getPostData() -> Future<Bool,ApiError>
    func getUsers()
    var postResult:CurrentValueSubject<[Post], Never> { get }
    var userResult: CurrentValueSubject<[User], Never> { get }
}
