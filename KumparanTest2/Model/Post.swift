//
//  Post.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation


struct Post: Codable {
    var userId: Int?
    var id: Int?
    var title, body: String?
}
