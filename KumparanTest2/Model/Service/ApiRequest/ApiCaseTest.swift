//
//  ApiCaseTest.swift
//  KumparanTest2
//
//  Created by bevan christian on 04/01/22.
//

import Foundation


enum ApiTestCase {
    case succes
    case invalidData
}

enum ApiError:Error {
    case decodeError
    case apiError
    case uknown
}
