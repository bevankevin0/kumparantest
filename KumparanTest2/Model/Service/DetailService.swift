//
//  CommentService.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine


class DetailService: DetailServiceProtocol {
   
    var subscription = Set<AnyCancellable>()
    var commentResult = CurrentValueSubject<[Comment], Never>([Comment()])
    var requestUtil: RequestUtils
    
    init(env: RequestUtils) {
        self.requestUtil = env
    }

    
    func getComments(idPost: String) {
        var urlComponent = URLComponents(string: UrlAddress.comment.urlString)
        urlComponent?.queryItems = [
            URLQueryItem(name: "postId", value: "\(idPost)"),
        ]
        guard let urlFix = try? URLRequest(url: (urlComponent?.url)!) else {return}
        requestUtil.request(urlComponent: urlComponent!, model: [Comment].self).sink { completion in
            switch completion {
            case .finished:
                print(completion)
            case .failure(let err):
                print("error get post \(err)")
            }
        } receiveValue: { data in
            print(data.count)
            self.commentResult.send(data)
        }.store(in: &subscription)
    }
}
