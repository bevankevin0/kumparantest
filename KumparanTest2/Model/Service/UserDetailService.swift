//
//  UserService.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine

class UserDetailService: UserDetailProtocol {
    
    var userResult = CurrentValueSubject<[User], Never>([User()])
    var albumResult = CurrentValueSubject<[Album], Never>([Album()])
    var albumPhotoResult = CurrentValueSubject<[AlbumPhoto], Never>([AlbumPhoto()])
    var subscription = Set<AnyCancellable>()
    var requestUtil: RequestUtils
    
    init(env: RequestUtils) {
        self.requestUtil = env
    }
    
    
    func getUserDetailData(id: Int) {
        getUserDetail(id: id)
        getAlbum(id: id)
        getAlbumPhoto(id: id)
    }
    
    
    func getUserDetail(id: Int) {
        var urlComponent2 = URLComponents(string: UrlAddress.user.urlString)
        urlComponent2?.queryItems = [
            URLQueryItem(name: "id", value: "\(id)"),
        ]
        
        requestUtil.request(urlComponent: urlComponent2!, model: [User].self).sink { completion in
            switch completion {
            case .finished:
                print(completion)
            case .failure(let err):
                print("error get post \(err)")
            }
        } receiveValue: { data in
            self.userResult.send(data)
        }.store(in: &subscription)
        
    }
    
    func getAlbum(id: Int) {
        let urlComponent2 = URLComponents(string: UrlAddress.albums(id).urlString)
        requestUtil.request(urlComponent: urlComponent2!, model: [Album].self).sink { completion in
            switch completion {
            case .finished:
                print(completion)
            case .failure(let err):
                print("error get post \(err)")
            }
        } receiveValue: { data in
            self.albumResult.send(data)
            
        }.store(in: &subscription)
        
    }
    
    func getAlbumPhoto(id: Int) {
        let urlComponent2 = URLComponents(string: UrlAddress.albumPhoto(id).urlString)
        requestUtil.request(urlComponent: urlComponent2!, model: [AlbumPhoto].self).sink { completion in
            switch completion {
            case .finished:
                print(completion)
            case .failure(let err):
                print("error get post \(err)")
            }
        } receiveValue: { data in
            self.albumPhotoResult.send(data)
            
        }.store(in: &subscription)
        
    }
    
}
