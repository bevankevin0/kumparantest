//
//  PostService.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation
import Combine


class HomeService: HomeServiceProtocol {
    var subscription = Set<AnyCancellable>()
    var postResult = CurrentValueSubject<[Post], Never>([Post()])
    var userResult = CurrentValueSubject<[User], Never>([User()])
    var requestUtil: RequestUtils
    
    init(env: RequestUtils) {
        self.requestUtil = env
    }
    
    func getPostData() -> Future<Bool,ApiError> {
        return Future() { [self] promise in
            let urlComponent = URLComponents(string: UrlAddress.post.urlString)
            requestUtil.request(urlComponent: urlComponent!, model: [Post].self).sink { completion in
                switch completion {
                case .finished:
                    print(completion)
                case .failure(_):
                    promise(.failure(.apiError))
                }
            } receiveValue: { data in
                print(data.count)
                self.postResult.send(data)
                if data.first?.title != nil && data.first?.body != nil {
                    promise(.success(true))
                } else {
                    promise(.failure(.apiError))
                }
                
            }.store(in: &subscription)
        }
    }
    
    func getUsers() {
        let urlComponent2 = URLComponents(string: UrlAddress.user.urlString)
        requestUtil.request(urlComponent: urlComponent2!, model: [User].self).sink { completion in
            switch completion {
            case .finished:
                print(completion)
            case .failure(let err):
                print("error get post \(err)")
            }
        } receiveValue: { data in
            self.userResult.send(data)
        }.store(in: &subscription)
    }
    
    
    
}
