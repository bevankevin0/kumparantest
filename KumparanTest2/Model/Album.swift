//
//  Album.swift
//  KumparanTest
//
//  Created by bevan christian on 03/01/22.
//

import Foundation

struct Album: Codable {
    var userId, id: Int?
    var title: String?
}
