//
//  KumparanTest2App.swift
//  KumparanTest2
//
//  Created by bevan christian on 04/01/22.
//

import SwiftUI

@main
struct KumparanTest2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
